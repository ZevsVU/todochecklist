//
//  TCViewController.m
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/26/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import "TCViewController.h"
#import "TCNetworkHandler.h"

@interface TCViewController ()

@end

@implementation TCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[TCNetworkHandler alloc] init] initRequestWithCompletionBlock:nil];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
