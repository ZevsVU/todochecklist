//
//  NSDictionary+NSDictinary_Json.m
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/27/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import "NSDictionary+NSDictinary_Json.h"

@implementation NSDictionary (NSDictinary_Json)

- (NSString*)toJSON:(BOOL)prettyPrint
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end
