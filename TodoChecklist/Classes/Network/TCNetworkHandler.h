//
//  TCNetworkHandler.h
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/26/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCNetworkHandler : NSObject

- (void)initRequestWithCompletionBlock:(void(^) (NSDictionary *responseDictionary))block;

@end
