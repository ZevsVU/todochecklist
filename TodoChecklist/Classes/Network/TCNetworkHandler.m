//
//  TCNetworkHandler.m
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/26/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import "TCNetworkHandler.h"
#import "NSData+Base64.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "NSDictionary+NSDictinary_Json.h"

@implementation TCNetworkHandler

- (void)initRequestWithCompletionBlock:(void(^) (NSDictionary *responseDictionary))block
{
    [self cgRequest];
}

- (void)asihttpRequest
{
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[self requestURL]];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    request.requestMethod = @"POST";
//    request.delegate = self;
    NSDictionary *requestDictionary = [self requestDictionary];
//    [request postBody]
    
    for (NSString *key in requestDictionary) {
        NSObject *value = [requestDictionary objectForKey:key];
        if (![key isEqualToString:@"action"]) {
            value = [NSData dataFromBase64String:value];
        }
        [request setPostValue:value forKey:key];
//        [request addRequestHeader:key value:value];
    }
//    NSData *requestDictionaryData = [NSJSONSerialization dataWithJSONObject:requestDictionary
//                                                                    options:NSJSONWritingPrettyPrinted
//                                                                      error:nil];
//    [request appendPostData:requestDictionaryData];
    [request startSynchronous];
    
    NSLog(@"%@",request.postBody);
    
    NSLog(@"%@", request.responseString);
    
//    ASIHTTPRequest *request;
//
////    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc] init];
////    [requestDict setObject:@"iSteve" forKey:@"UserId"];
////    [requestDict setObject:@"1" forKey:@"CompanyCode"];
////    [requestDict setObject:@"IN" forKey:@"LineOfBusiness"];
////    [requestDict setObject:@"M" forKey:@"LineOfBusinessClassification"];
////    [requestDict setObject:pricingVariablesListString forKey:@"CarQuoteString"];
////    [request appendPostData:[[self requestDictionary] toJSON:YES]];
//    NSLog(@"%@", [[self requestDictionary] toJSON:YES]);
    
}

- (void)nsRequest
{
    NSURL *url = [self requestURL];
    NSDictionary *requestDictionary = [self requestDictionary];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSString *parametrsString = @"";
    for (NSString *key in requestDictionary) {
        parametrsString = [NSString stringWithFormat:@"%@%@=%@&", parametrsString, key, [requestDictionary objectForKey:key]];
//        [request setValue:[requestDictionary objectForKey:key] forKey:key];
    }
    parametrsString = [parametrsString substringToIndex:[parametrsString length] - 1];
    
    [request setHTTPBody:[parametrsString dataUsingEncoding:NSUTF8StringEncoding]];

//    NSString *originalString = [NSString stringWithFormat:parametrsString];
//    NSData *data = [NSData dataFromBase64String:originalString];
//    NSLog([data base64EncodedString]);
//    request.HTTPBody = data;
    
//    NSData *requestData = [NSKeyedArchiver archivedDataWithRootObject:requestDictionary];
//    request.HTTPBody = [requestData base64EncodedDataWithOptions:NSUTF8StringEncoding];
    
    NSURLResponse *response;
    
    NSData *resData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    if (resData) {
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:resData options:kNilOptions error:nil];
        NSLog(@"%@", dic);
    }
    NSLog(@"%@", response);
    
    //    NSLog(@"%@", [[UIDevice currentDevice] name]);
}

- (NSURL*)requestURL
{
    return [NSURL URLWithString:@"https://dev-auth.simplexsolutionsinc.com?action=login"];
}

#pragma mark-  CGRequest

- (void)cgRequest
{
    
}

- (void)buildMessage
{
    CFStringRef bodyString = CFSTR(""); // Usually used for POST data
    CFDataRef bodyData = CFStringCreateExternalRepresentation(kCFAllocatorDefault,
                                                              bodyString, kCFStringEncodingUTF8, 0);
    
    CFStringRef headerFieldName = CFSTR("X-My-Favorite-Field");
    CFStringRef headerFieldValue = CFSTR("Dreams");
    
    CFStringRef url = CFSTR("http://www.apple.com");
    CFURLRef myURL = CFURLCreateWithString(kCFAllocatorDefault, url, NULL);
    
    CFStringRef requestMethod = CFSTR("GET");
    CFHTTPMessageRef myRequest =
    CFHTTPMessageCreateRequest(kCFAllocatorDefault, requestMethod, myURL,
                               kCFHTTPVersion1_1);
    
    CFDataRef bodyDataExt = CFStringCreateExternalRepresentation(kCFAllocatorDefault, bodyData, kCFStringEncodingUTF8, 0);
    CFHTTPMessageSetBody(myRequest, bodyDataExt);
    CFHTTPMessageSetHeaderFieldValue(myRequest, headerFieldName, headerFieldValue);
    CFDataRef mySerializedRequest = CFHTTPMessageCopySerializedMessage(myRequest);
    
    
    
    CFRelease(myRequest);
    CFRelease(myURL);
    CFRelease(url);
    CFRelease(mySerializedRequest);
    myRequest = NULL;
    mySerializedRequest = NULL;
}

#pragma mark - Data

- (NSDictionary*)requestDictionary
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
//            @"login", @"action",
            @"com.braininstock.ToDoChecklist", @"service",
            @"pink007@mailinator.com", @"login",
            @"q", @"password",
            [self appUID], @"deviceid",
            [self deviceName],@"device",
            @"Android", @"platform",//TODO:?
            @"7.0", @"platformversion",
            @"3.1",@"appversion",
            @"en_US",@"locale",
            [self timezoneString],@"timezone",
            nil];
}

- (NSString*)appVersion
{
    return @"3.1";
}

- (NSString*)osVersion
{
    return @"";
}

- (NSString*)deviceName
{
    return [[UIDevice currentDevice] name];
}

- (NSString*)timezoneString
{
    return [NSString stringWithFormat:@"%+4d" ,[[NSTimeZone localTimeZone] secondsFromGMT]];
}

- (NSString *)appUID
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%@", response);
}


@end
