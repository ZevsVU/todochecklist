//
//  NSDictionary+NSDictinary_Json.h
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/27/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictinary_Json)

- (NSString*)toJSON:(BOOL)prettyPrint;

@end
