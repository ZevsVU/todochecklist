//
//  TCAppDelegate.h
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/26/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
