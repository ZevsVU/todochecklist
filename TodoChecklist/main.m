
//  main.m
//  TodoChecklist
//
//  Created by Valdimir Nuzhdin on 8/26/14.
//  Copyright (c) 2014 Valdimir Nuzhdin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TCAppDelegate class]));
    }
}
